<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'            => 'user',
            'email'           => 'user@user.com',
            'password'        => '123456',
        ]);

        User::create([
            'name'            => 'user1',
            'email'           => 'user1@user.com',
            'password'        => '123456',
        ]);

        User::create([
            'name'            => 'user2',
            'email'           => 'user2@user.com',
            'password'        => '123456',
        ]);

        User::create([
            'name'            => 'user3',
            'email'           => 'user3@user.com',
            'password'        => '123456',
        ]);

        User::create([
            'name'            => 'user4',
            'email'           => 'user4@user.com',
            'password'        => '123456',
        ]);

    }
}
