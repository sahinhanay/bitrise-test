<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect,Response,DB,Config;
use DataTables;
use App\Models\Admin;

class AdminController extends Controller
{
    protected $routePrefix = 'admin.admin';
    protected $model       = User::class;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('admin.admin.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Admin::updateOrCreate(
            ['id' => $request->id],
            ['name' => $request->name, 'detail' => $request->email]);

        return response()->json(['success'=>'Product saved successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $model = Admin::find($id);

        return redirect()->back();

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Admin::find($id);
        if($id == 1) {
            return redirect()->back();
        }
        $model->delete();

        return redirect()->back();
    }

    public function adminsList()
    {
        $query = Admin::query();

        return datatables()->eloquent($query)
            ->setRowAttr([
            'data-id' => function ($model) {
                return $model->id;
            },
            ])
            ->rawColumns(['actions'])
            ->addColumn('actions', function ($model) {
                return view('admin.include.datatable.action_button')
                    ->with('routePrefix', $this->routePrefix)
                    ->with('model', $model); 
                
            })
            ->make(true);
    }
}
