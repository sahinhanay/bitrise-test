<?php

Route::get('login', ['as' => 'login', 'uses' => 'AuthController@getLogin']);
Route::post('login', ['as' => 'login', 'uses' => 'AuthController@postLogin']);
Route::get('logout', ['as' => 'logout', 'uses' => 'AuthController@getLogout']);


Route::group(['middleware' => 'auth:admin'], function() {
    // Dashboard
    Route::get('/', ['as' => 'home.index', 'uses' => 'HomeController@index']);

    // Admin
    Route::get('/admin-datatable',['as' => 'admin.datatable', 'uses' =>  'AdminController@adminsList']);
    Route::resource('admin', 'AdminController');
    
    // Users
    Route::get('/user-datatable',['as' => 'user.datatable', 'uses' =>  'UserController@usersList']);
    Route::resource('user', 'UserController');
});
