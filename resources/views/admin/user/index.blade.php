@extends('admin.app')
@section('content')
  @include('admin.include.datatable.index')
@endsection
@section('js')
	<script type="text/javascript">
	  $(document).ready( function () {

			$('#laravel_datatable').DataTable({
				processing: true,
				serverSide: true,
				ajax: "{{ route('admin.user.datatable') }}",
				columns: [
					{ data: 'id', name: 'id' },
					{ data: 'name', name: 'name' },
					{ data: 'email', name: 'email' },
					{ data: 'created_at', name: 'created_at' },
					{ data: 'actions', name: 'actions' },
				]
			});
		});
	</script>
@endsection

@section('css')

@endsection
