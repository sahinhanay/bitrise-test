 <!-- sidebar menu -->
 <div id="sidebar-menu" class="main_menu_side main_menu">
     <div class="menu_section">
         <ul class="nav side-menu">
			 <li><a href="{{ route('admin.home.index') }}"><i class="fa fa-home"></i>Anasayfa</a></li>
			 <li><a href="{{ route('admin.admin.index') }}"><i class="fa fa-users"></i>Yöneticiler</a></li>
			 <li><a href="{{ route('admin.user.index') }}"><i class="fa fa-users"></i>Kullanıcılar</a></li>
		 </ul>
     </div>
 </div>
