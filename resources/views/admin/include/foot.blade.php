    <!-- jQuery -->
    <script src="/backend/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="/backend/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="/backend/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="/backend/vendors/nprogress/nprogress.js"></script>
    
    <!-- Custom Theme Scripts -->
    <script src="/backend/build/js/custom.min.js"></script>
	@yield('js')
  </body>
</html>
