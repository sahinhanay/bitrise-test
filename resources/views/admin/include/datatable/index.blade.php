<div class="right_col" role="main">
  <div class="container">
    <h2>DataTable</h2>
          <div style="display: flex; justify-content: flex-end">
              <div class="btn-group">
                  <a href="{{ route('admin.admin.create')}}" class="btn btn-primary active">
                      <i class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></i> Oluştur
                  </a>
              </div>
          </div>
          <table class="table table-bordered" id="laravel_datatable">
      <thead>
        <tr>
          <th>Id</th>
          <th>Name</th>
          <th>Email</th>
          <th>Created at</th>
          <th>Action</th>
        </tr>
      </thead>
    </table>
  </div>
</div>
