<div class="btn-group">
	{{ Form::open(['route' => [$routePrefix . '.edit', $model->id],  'method' => 'GET']) }}
	<button type="submit" class="btn btn-primary">
		<i class="glyphicon" aria-hidden="true"></i> Düzenle
    </button>
	{{ Form::close() }}
{{ Form::open(['route' => [$routePrefix . '.destroy', $model->id],  'method' => 'DELETE']) }}
	<button type="submit" class="btn btn-primary">
		<i class="glyphicon" aria-hidden="true"></i> Sil
	</button>
{{ Form::close() }}
</div>
