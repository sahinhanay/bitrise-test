<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Admin Paneli </title>

		<!-- Bootstrap -->
		<link href="/backend/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
		<!-- Font Awesome -->
		<link href="/backend/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
		<!-- NProgress -->
		<link href="/backend/vendors/nprogress/nprogress.css" rel="stylesheet">
		<!-- Animate.css -->
		<link href="/backend/vendors/animate.css/animate.min.css" rel="stylesheet">

		<!-- Custom Theme Style -->
		<link href="/backend/build/css/custom.min.css" rel="stylesheet">
	</head>

	<body class="login">
		<div>
			<a class="hiddenanchor" id="signup"></a>
			<a class="hiddenanchor" id="signin"></a>

			<center>
				<div class="m-login__logo" >
					<img src="/backend/assets/app/media/img/logo/logo.jpeg" style="max-width: 200px;">
				</div>                          
			</center>
			<div class="login_wrapper">
				<div class="animate form login_form">
					<section class="login_content">
                        <h1>Etiket Admin Paneli</h1>
						{!! Form::open(['route' => 'admin.login', 'class' => 'm-login__form m-form', 'id' => 'login-form', 'data-redirect' => route('admin.home.index')]) !!}
						<div class="form-group m-form__group">
							{!! Form::email('email', null, ['class' => 'form-control m-input', 'placeholder' => trans('admin.login.email'), 'autocomplate' => 'off', 'required']) !!}
						</div>
						<div class="form-group m-form__group">
							{!! Form::password('password', ['class' => 'form-control m-input m-login__form-input--last', 'placeholder' => trans('admin.login.password'), 'required']) !!}
						</div>
						<div class="m-login__form-action">
							<button id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">{!! trans('admin.login.sign-in') !!}</button>
						</div>
						{!! Form::close() !!}
					</section>
				</div>
			</div>
		</div>
	</body>
</html>
