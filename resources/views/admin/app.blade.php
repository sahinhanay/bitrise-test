<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Admin Panel</title>

		<!-- Bootstrap -->
		<link href="/backend/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
		<!-- Font Awesome -->
		<link href="/backend/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
		<!-- NProgress -->
		<link href="/backend/vendors/nprogress/nprogress.css" rel="stylesheet">

		<!-- Custom Theme Style -->
		<link href="/backend/build/css/custom.min.css" rel="stylesheet">
        <!-- Datatable -->
		<link  href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">

		@yield('css')
	</head>

	<body class="nav-md">
		<div class="container body">
			<div class="main_container">
				<div class="col-md-3 left_col">
					<div class="left_col scroll-view">
						<div class="navbar nav_title" style="border: 0;">
							<a href="{{ route('admin.home.index') }}" class="site_title"><img src="/backend/assets/app/media/img/logo/logo.jpeg" style="max-width: 50px;">
								<span>Admin Panel</span></a>
						</div>

						<div class="clearfix"></div>

						<!-- menu profile quick info -->
						<div class="profile clearfix">

							<div class="clearfix"></div>
						</div>
						<!-- /menu profile quick info -->
						<br />

						<!-- sidebar menu -->
						<div id="sidebar-menu" class="main_menu_side main_menu">
							<div class="menu_section">
								<ul class="nav side-menu">
									<li><a href="{{ route('admin.home.index') }}"><i class="fa fa-home"></i>Anasayfa</a></li>
									<li><a href="{{ route('admin.admin.index') }}"><i class="fa fa-users"></i>Yöneticiler</a></li>
									<li><a href="{{ route('admin.user.index') }}"><i class="fa fa-users"></i>Kullanıcılar</a></li>
								</ul>
							</div>
						</div>
						<!-- /sidebar menu -->
					</div>
				</div>

				<!-- top navigation -->
				<div class="top_nav">
					<div class="nav_menu">
						<nav>
							<div class="nav toggle">
								<a id="menu_toggle"><i class="fa fa-bars"></i></a>
							</div>

							<ul class="nav navbar-nav navbar-right">
								<li class="">
									<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
										<img src="images/img.jpg" alt="">Admin
										<span class=" fa fa-angle-down"></span>
									</a>
									<ul class="dropdown-menu dropdown-usermenu pull-right">
										<li><a href="javascript:;"> Profil</a></li>
										<li><a href="{{ route('admin.logout') }}"><i class="fa fa-sign-out pull-right"></i> Çıkış Yap</a></li>
									</ul>
								</li>
							</ul>
		</li>
								</ul>
						</nav>
					</div>
				</div>
				<!-- /top navigation -->
				@yield('content')
				<!-- footer content -->
				<footer>
					<div class="pull-right">
                        Admin Panel 2019
                    </div>
					<div class="clearfix"></div>
				</footer>
				<!-- /footer content -->
			</div>
		</div>
		<!-- jQuery -->
		<script src="/backend/vendors/jquery/dist/jquery.min.js"></script>
		<!-- Bootstrap -->
		<script src="/backend/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
		<!-- FastClick -->
		<script src="/backend/vendors/fastclick/lib/fastclick.js"></script>
		<!-- NProgress -->
		<script src="/backend/vendors/nprogress/nprogress.js"></script>

		<!-- Custom Theme Scripts -->
		<script src="/backend/build/js/custom.min.js"></script>
		<!-- Datatable -->
		<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
		@yield('js')
	</body>
</html>
